# Image Machine App - iOS

## Author

Created and Developed by: Agung Setiawan (agung079@gmail.com)

## Requirement to Succesfully Build and Run the App

- XCode 10.0 or later
- Swift 4.2 or later
- Physical iPhone device with iOS 12.0 or later (Required to test the QR reader, simulator doesn't have camera)
- An active apple developer account (Required to build the app to the physical device)

## Step to Run the App

- Clone the project
- Open .xcworkspace file
- Setup to the correct active apple developer account
- Navigate "Product -> Run"
