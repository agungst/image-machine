//
//  Extension.swift
//  Image Machine
//
//  Created by Agung Setiawan on 30/11/18.
//  Copyright © 2018 Agung Setiawan. All rights reserved.
//

import UIKit
import Photos

extension UIViewController {
    
    func checkPhotoLibraryPermission() {
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:
            //handle authorized status
            break
        case .denied, .restricted :
            //handle denied status
            PHPhotoLibrary.requestAuthorization { status in }
        case .notDetermined:
            // ask for permissions
            PHPhotoLibrary.requestAuthorization { status in }
        }
    }
    
}
