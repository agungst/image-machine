//
//  MachineImageCollectionViewCell.swift
//  Image Machine
//
//  Created by Agung Setiawan on 30/11/18.
//  Copyright © 2018 Agung Setiawan. All rights reserved.
//

import UIKit

class MachineImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
}
