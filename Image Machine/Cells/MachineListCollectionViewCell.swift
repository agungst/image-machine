//
//  MachineListCollectionViewCell.swift
//  Image Machine
//
//  Created by Agung Setiawan on 29/11/18.
//  Copyright © 2018 Agung Setiawan. All rights reserved.
//

import UIKit

class MachineListCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    
}
