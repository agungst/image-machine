//
//  ListViewController.swift
//  Image Machine
//
//  Created by Agung Setiawan on 29/11/18.
//  Copyright © 2018 Agung Setiawan. All rights reserved.
//

import UIKit
import RealmSwift
import SwifterSwift

class ListViewController: UIViewController {
    
    var machines: [MachineObject] = []
    let realm = try! Realm()

    @IBOutlet weak var colView: UICollectionView!
    
    @IBAction func addMachineTapped(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: String(describing: FormViewController.self))
        navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func sortNameTapped(_ sender: Any) {
        machines = realm.objects(MachineObject.self).map { $0 }.sorted(by: { (first, second) -> Bool in
            return first.machineName.lowercased() < second.machineName.lowercased()
        })
        colView.reloadData()
    }
    
    @IBAction func sortTypeTapped(_ sender: Any) {
        machines = realm.objects(MachineObject.self).map { $0 }.sorted(by: { (first, second) -> Bool in
            return first.machineType.lowercased() < second.machineType.lowercased()
        })
        colView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        colView.delegate = self
        colView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        machines = realm.objects(MachineObject.self).map { $0 }
        colView.reloadData()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ListViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return machines.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: MachineListCollectionViewCell.self), for: indexPath) as! MachineListCollectionViewCell
        
        cell.nameLabel.text = machines[indexPath.item].machineName
        cell.typeLabel.text = machines[indexPath.item].machineType
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: SwifterSwift.screenWidth-32, height: 82)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: String(describing: DetailViewController.self)) as! DetailViewController
        vc.machine = machines[indexPath.item]
        navigationController?.pushViewController(vc)
    }
    
}
