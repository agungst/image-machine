//
//  ImageViewController.swift
//  Image Machine
//
//  Created by Agung Setiawan on 29/11/18.
//  Copyright © 2018 Agung Setiawan. All rights reserved.
//

import UIKit
import RealmSwift
import Photos

class ImageViewController: UIViewController {

    var machine: MachineObject!
    var urlString: String!
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBAction func deleteTapped(_ sender: Any) {
        if let index = self.machine.machineImages.index(of: self.urlString) {
            let realm = try! Realm()
            
            try! realm.write {
                self.machine.machineImages.remove(at: index)
            }
        }
        self.navigationController?.popViewController()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        checkPhotoLibraryPermission()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let asset = PHAsset.fetchAssets(withLocalIdentifiers: [urlString], options: nil).firstObject {
            let requestImageOption = PHImageRequestOptions()
            requestImageOption.deliveryMode = PHImageRequestOptionsDeliveryMode.highQualityFormat
            
            let manager = PHImageManager.default()
            manager.requestImage(for: asset, targetSize: PHImageManagerMaximumSize, contentMode:PHImageContentMode.default, options: requestImageOption) { (image:UIImage?, _) in
                self.imageView.image = image ?? UIImage()
            }
            
        }

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
