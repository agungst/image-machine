//
//  FormViewController.swift
//  Image Machine
//
//  Created by Agung Setiawan on 29/11/18.
//  Copyright © 2018 Agung Setiawan. All rights reserved.
//

import UIKit
import SwifterSwift
import RealmSwift
import ActionSheetPicker_3_0

class FormViewController: UIViewController, UITextFieldDelegate {

    var machine: MachineObject?
    var machineLastMaintenanceDateSelected = Date()
    
    @IBOutlet weak var machineIdField: UITextField!
    @IBOutlet weak var machineNameField: UITextField!
    @IBOutlet weak var machineTypeField: UITextField!
    @IBOutlet weak var machineCodeField: UITextField!
    @IBOutlet weak var machineLastMaintenanceDateField: UITextField!
    
    @IBAction func saveTapped(_ sender: Any) {
        
        guard let machine = machine else { return }

        let realm = try! Realm()

        try! realm.write {
            machine.machineName = machineNameField.text ?? ""
            machine.machineType = machineTypeField.text ?? ""
            machine.machineCode = machineCodeField.text?.int ?? 0
            machine.machineLastMaintenanceDate = machineLastMaintenanceDateSelected
            realm.add(machine)
        }
        
        navigationController?.popViewController()

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        machineLastMaintenanceDateField.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let machine = machine {
            machineIdField.text = machine.machineId
            machineNameField.text = machine.machineName
            machineTypeField.text = machine.machineType
            machineCodeField.text = machine.machineCode.string
            machineLastMaintenanceDateField.text = machine.machineLastMaintenanceDate.string(withFormat: "dd MMM yyyy")
            machineLastMaintenanceDateSelected = machine.machineLastMaintenanceDate
        } else {
            machine = MachineObject()
            guard let machine = machine else { return }
            machineIdField.text = machine.machineId
            machineLastMaintenanceDateField.text = machine.machineLastMaintenanceDate.string(withFormat: "dd MMM yyyy")
            machineLastMaintenanceDateSelected = machine.machineLastMaintenanceDate
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        if textField == machineLastMaintenanceDateField {
            ActionSheetDatePicker.show(withTitle: "Pick a date", datePickerMode: .date, selectedDate: Date(), doneBlock: { (picker, value, view) in
                self.machineLastMaintenanceDateSelected = value as? Date ?? Date()
                self.machineLastMaintenanceDateField.text = self.machineLastMaintenanceDateSelected.string(withFormat: "dd MMM yyyy")
            }, cancel: { (picker) in
                return
            }, origin: view)
            return false
        } else {
            return true
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
