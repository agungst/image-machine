//
//  DetailViewController.swift
//  Image Machine
//
//  Created by Agung Setiawan on 29/11/18.
//  Copyright © 2018 Agung Setiawan. All rights reserved.
//

import UIKit
import RealmSwift
import SwifterSwift
import BSImagePicker
import Photos

class DetailViewController: UIViewController {

    var machine: MachineObject!
    
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var qrLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var colView: UICollectionView!
    
    @IBAction func editTapped(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: String(describing: FormViewController.self)) as! FormViewController
        vc.machine = machine
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func deleteTapped(_ sender: Any) {
        let realm = try! Realm()

        try! realm.write {
            realm.delete(machine)
        }
        navigationController?.popViewController()
    }
    
    @IBAction func addImageTapped(_ sender: Any) {
        let vc = BSImagePickerViewController()
        vc.maxNumberOfSelections = 10
        
        bs_presentImagePickerController(vc, animated: true,
        select: { (asset: PHAsset) -> Void in
            // User selected an asset.
            // Do something with it, start upload perhaps?
        }, deselect: { (asset: PHAsset) -> Void in
            // User deselected an assets.
            // Do something, cancel upload?
        }, cancel: { (assets: [PHAsset]) -> Void in
            // User cancelled. And this where the assets currently selected.
        }, finish: { (assets: [PHAsset]) -> Void in
            // User finished with these assets
            DispatchQueue.main.sync {
                var identifiers: [String] = []
                for asset in assets {
                    identifiers.append(asset.localIdentifier)
                }
                
                let realm = try! Realm()
                
                try! realm.write {
                    self.machine.machineImages.append(objectsIn: identifiers)
                }
                
                self.colView.reloadData()
            }

        }, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        colView.delegate = self
        colView.dataSource = self
        checkPhotoLibraryPermission()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        idLabel.text = machine.machineId
        nameLabel.text = machine.machineName
        typeLabel.text = machine.machineType
        qrLabel.text = machine.machineCode.string
        dateLabel.text = machine.machineLastMaintenanceDate.string(withFormat: "dd MMM yyyy")
        colView.reloadData()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DetailViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return machine.machineImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: MachineImageCollectionViewCell.self), for: indexPath) as! MachineImageCollectionViewCell

        if let asset = PHAsset.fetchAssets(withLocalIdentifiers: [machine!.machineImages[indexPath.item]], options: nil).firstObject {
            let requestImageOption = PHImageRequestOptions()
            requestImageOption.deliveryMode = PHImageRequestOptionsDeliveryMode.fastFormat
            
            let manager = PHImageManager.default()
            manager.requestImage(for: asset, targetSize: PHImageManagerMaximumSize, contentMode:PHImageContentMode.default, options: requestImageOption) { (image:UIImage?, _) in
                cell.imageView.image = image ?? UIImage()
            }

        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 48, height: 48)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: String(describing: ImageViewController.self)) as! ImageViewController
        vc.machine = machine
        vc.urlString = machine!.machineImages[indexPath.item]
        navigationController?.pushViewController(vc)
    }
    
}
