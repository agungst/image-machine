//
//  MachineObject.swift
//  Image Machine
//
//  Created by Agung Setiawan on 29/11/18.
//  Copyright © 2018 Agung Setiawan. All rights reserved.
//

import Foundation
import RealmSwift

class MachineObject: Object {
    @objc dynamic var machineId = NSUUID().uuidString
    @objc dynamic var machineName = ""
    @objc dynamic var machineType = ""
    @objc dynamic var machineCode = 0
    @objc dynamic var machineLastMaintenanceDate = Date()
    let machineImages = List<String>()
    
    override static func primaryKey() -> String? {
        return "machineId"
    }

}
